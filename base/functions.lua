-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- GLOBAL
-- ****************************************

--- Remplace l'instruction switch
--[[
  USAGE:
  switch(case) {
    [1] = function () print"number 1!" end,
    [2] = math.sin,
    [false] = function (a) return function (b) return (a or b) and not (a and b) end end,
    Default = function (x) print"Look, Mom, I can differentiate types!" end, -- ["Default"] ;)
    [Default] = print,
    [Nil] = function () print"I must've left it in my other jeans." end,
  }
]]
Default, Nil = {}, function () end --- for uniqueness
function switch (pIndex)
  return setmetatable({pIndex}, {
    __call = function (t, cases)
      local item = #t == 0 and Nil or t[1]
      return (cases[item] or cases[Default] or Nil)(item)
    end
  })
end -- switch

--- Itérateur pour une table FINIE
--[[
USAGE 1:
  t = {10, 20, 30}
  iter = iterator(t)    -- creates the iterator
  while true do
    local element = iter()   -- calls the iterator
    if element == nil then break end
    print(element)
  end
USAGE 2:
  t = {10, 20, 30}
  for element in iterator(t) do
    print(element)
  end
]]
function iterator (pTable)
  local i = 0
  local n = table.getn(pTable)
  return function ()
    i = i + 1
    if i <= n then return pTable[i] end
  end
end -- iterator

-- ****************************************
-- CHAINES
-- ****************************************
--- function interne
function _titleCase (first, rest)
   return first:upper()..rest:lower()
end
-- met la 1ere lettre des mots d'une chaîne en majuscule
function string.titleCase (str)
  return string.gsub(str, "(%a)([%w_']*)", _titleCase)
end

-- ****************************************
-- MATHS
-- ****************************************

--- Retourne l'angle en radiant de la droite passant par 2 points (utile pour les déplacements en X et Y)
-- Ajoutée au module math
-- usage:
-- pSprite.angle = math.angle(pSprite.x, pSprite.y, player.x, player.y)
-- vX = speed * math.cos(pSprite.angle)
-- vY = speed * math.sin(pSprite.angle)
-- @param pX1 position X du point 1
-- @param pY1 position Y du point 1
-- @param pX2 position X du point 2
-- @param pY2 position Y du point 2
-- @return angle en radiant
function math.angle (pX1, pY1, pX2, pY2)
  return math.atan2(pY2 - pY1, pX2 - pX1)
end -- math.angle

-- Retourne la distance entre 2 points.
-- Ajoutée au module math
-- @param pX1 position X du point 1
-- @param pY1 position Y du point 1
-- @param pX2 position X du point 2
-- @param pY2 position Y du point 2
-- @return distance en pixel
function math.dist (x1, y1, x2, y2)
  return ((x2 - x1)^ 2  + (y2 - y1) ^ 2) ^ 0.5
end

-- ****************************************
-- TABLES
-- ****************************************

--- Mélange le contenu d'une table
-- @param pTable table à mélanger
-- @return la table mélangée
-- @see https://forums.coronalabs.com/topic/195-random-table-shuffle/
function table.shuffle (pTable)
  local count = #pTable
  math.randomseed(love.timer.getTime())
  for i = 1, (count * 20) do
    local index0 = math.random(1, count)
    local index1 = math.random(1, count)
    local temp = pTable [index0]
    pTable[index0] = pTable [index1]
    pTable[index1] = temp
  end
  return pTable
end -- table.shuffle

--- merge le contenu de 2 tables
-- @param pFirstTable 1ere table. Son contenu sera modifié
-- @param secondTable 2e table
-- @return la 1ere table avec en plus le contenu de la seconde
function table.merge (pFirstTable, pSecondTable)
  for k,v in pairs(pSecondTable) do
    pFirstTable[k] = v
  end
  return pFirstTable
end -- table.merge

-- ****************************************
-- DEBUG et LOG
-- ****************************************

--- Affiche le contenu d'une variable
-- @param pVar variable à afficher
function dump (pVar)
  if (pVar == nil) then
    print "NIL"
  elseif (type(pVar) == "table") then
    for k, v in pairs(pVar) do
      print(tostring(k).." = "..tostring(v))
    end
  else
    print(pVar)
  end
end -- dump

--- Retourne TRUE si une une variable est égale à une valeur donnée, FALSE sinon et affiche un message optionnel
-- @param pVar variable à tester
-- @param pValue valeur à comparer
-- @param pMessage (OPTIONNEL) texte à afficher quand l'assertion est VRAIE
-- @return true si l'assertion est vraie, false sinon
function assertEqual (pVar, pValue, pMessage)
  if (pVar == pValue) then
    if (pMessage ~= nil) then
      print ("assertEqual: ", pMessage, " Value=", tostring(pValue))
    end
    return true
  else
    return false
  end
end -- assertEqual

--- Quitte le jeu si une une variable est égale à une valeur donnée, et affiche un message optionnel
-- @param pVar variable à tester
-- @param pValue valeur à comparer
-- @param pMessage (OPTIONNEL) texte à afficher quand l'assertion est VRAIE
function assertEqualQuit (pVar, pValue, pMessage)
  if (pVar == pValue) then
    if (pMessage ~= nil) then
       print ("assertEqualQuit: ", pMessage, " Value=", tostring(pValue))
    end
    print ("ARRET SUR VALIDATION D'UNE ASSERTION")
    love.event.push('quit')
  end
end -- assertEqualQuit

--- Logue (Affiche) un message d'erreur
-- @param pMessage texte à loguer
-- @param mustQuit (OPTIONNEL) si vrai, termine le jeu après avoir afficher le message
function logError (pMessage, pMustQuit)
  print (pMessage)
  if (pMustQuit) then love.window.close() end
end -- logError

--- Logue (Affiche) des contenus pour du debug
-- @param Paramètres dynamiques
function debugMessage (...)
  if (DEBUG_MODE >= 1) then
    print(os.date().."# INFO #", ...)
  end
end -- debugMessage

--- Logue (Affiche) des contenus pour du debug lors de l'entrée dans une fonction SANS les appels qui peuvent remplir le log
-- @param Paramètres dynamiques
function debugFunctionEnter (...)
  if (DEBUG_MODE >= 4) then
    print(os.date().."# ENTREE FONCTION #", ...)
  end
end -- debugFunctionEnter

--- Logue (Affiche) des contenus pour du debug lors de l'entrée dans une fonction AVEC les appels qui peuvent remplir le log
-- @param Paramètres dynamiques
function debugFunctionEnterNL (...)
  if (DEBUG_MODE >= 5) then
    print(os.date().."# ENTREE FONCTION #", ...)
  end
end -- debugFunctionEnterNL

---Affiche un point rouge au coordonnées données
-- @param pX position X du point
-- @param pY position Y du point
function debugPoint (pX, pY)
  love.graphics.setColor(255, 0, 0, 255)
  love.graphics.rectangle("fill", pX, pY, 10, 10)
  love.graphics.setColor(255, 255, 255, 255)
end -- debugPoint

--- Logue (Affiche) des contenus d'information
-- @param Paramètres dynamiques
function logMessage (...)
  print(os.date().."# LOG #", ...)
end -- logMessage

-- ****************************************
-- IMAGES
-- ****************************************

--- Créer un objet image (love) en vérifiant si le fichier existe
-- @param filename Nom du fichier
-- @return l'objet image ou nil si le fichier n'existe pas
function newImageIfExists (filename)
  if (filename ~= nil and filename ~= "" and love.filesystem.isFile(filename)) then
    return love.graphics.newImage(filename)
  else
    return nil
  end
end -- newImageIfExists

--- Ajoute les images contenues dans un dossier à une liste
-- @param pFolder le chemin du dossier contenant les images
-- @param pExt (OPTIONNEL) extension des fichier à prendre en compte. Si absent,".png" sera utilisé
-- @return une table contenant les images associées aux fichiers images trouvés dans le dossier
function addImages (pFolder, pExt)
  -- debugFunctionEnter("addImages ", pFolder, pExt)
  if (pExt == nil) then pExt  = ".png" end
  local key, value, fileNameLen, fileExt
  local imageList = {}
  local allFiles = {}
  local pExtLen = string.len(pExt)

  pExt = string.lower(pExt)
  local files = love.filesystem.getDirectoryItems(pFolder)
  -- liste tous les fichiers du répertoire
  for key, value in pairs(files) do
    local fileName = pFolder.."/"..value
    if (love.filesystem.isFile(fileName)) then
      -- compare l'extension du fichier et celle passée en paramètre
      fileNameLen = string.len(fileName)
      fileExt = string.sub(fileName, -pExtLen)
      fileExt = string.lower(fileExt)
      -- si elle correspondent, on ajoute le fichier à la liste
      if (fileExt == pExt) then table.insert(allFiles, fileName) end
    end
  end
  -- trie les nom de fichiers (pour ordonner les images de l'animation)
  table.sort(allFiles)
  -- ajout les images à la table des images retournée
  for key, value in ipairs(allFiles) do
    table.insert(imageList, love.graphics.newImage(value))
  end

  return imageList
end -- addImages


-- ****************************************
-- AFFICHAGE
-- ****************************************

--- Affiche un texte à l'écran en le positionnant relativement dans le viewport
-- @param pText texte à afficher
-- @parampFont police à utiliser.
-- @param pCONST_POSITON (OPTIONNEL) position relative à l'écran (voir les constantes POS_xxx). Le texte sera centré à de l'écran par défaut
-- @param pOffsetX (OPTIONNEL) offset à ajouter en X
-- @param pOffsetY (OPTIONNEL) offset à ajouter en Y
-- @param pColor (OPTIONNEL) couleur du texte. La couleur blanc sera utilisée. par défaut
-- @param pTextWidth (OPTIONNEL) largeur du texte. Si absent la valeur sera calculée automatiquement.
-- @param pTextHeight (OPTIONNEL) hauteur du texte. Si absent la valeur sera calculée automatiquement.
function displayText (pText, pFont, pCONST_POSITON, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight)
  ---- debugFunctionEnterNL("displayText ", pText, pFont, pCONST_POSITON, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight) -- ATTENTION  cet appel peut remplir le log

  if (pCONST_POSITON == nil) then pCONST_POSITON = POS_CENTER end
  if (pOffsetX == nil) then pOffsetX = 0 else pOffsetX = (pOffsetX / 1) end
  if (pOffsetY == nil) then pOffsetY = 0 else pOffsetY = (pOffsetY / 1) end
  if (pColor == nil) then pColor = {{255, 255, 255, 255}} end
  if (pTextWidth == nil) then pTextWidth = pFont:getWidth(pText) else pTextWidth = (pTextWidth / 1) end -- note la font tient déjà compte de l'échelle
  if (pTextHeight == nil) then pTextHeight = pFont:getHeight(pText) else pTextHeight = (pTextHeight / 1) end -- note la font tient déjà compte de l'échelle
  love.graphics.setFont(pFont)

  love.graphics.setColor(pColor[1], pColor[2], pColor[3], pColor[4])

  local Xmax = screenWidth - pTextWidth
  local Ymax = screenHeight - pTextHeight
  local drawX, drawY

  switch (pCONST_POSITON) {
    [POS_TOP_LEFT]      = function () drawX = pOffsetX           ; drawY = pOffsetY            end,
    [POS_TOP_RIGHT]     = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY            end,
    [POS_TOP_CENTER]    = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY            end,
    [POS_CENTER]        = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY + Ymax / 2 end,
    [POS_CENTER_LEFT]   = function () drawX = pOffsetX           ; drawY = pOffsetY + Ymax / 2 end,
    [POS_CENTER_RIGHT]  = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY + Ymax / 2 end,
    [POS_BOTTOM_LEFT]   = function () drawX = pOffsetX           ; drawY = pOffsetY + Ymax     end,
    [POS_BOTTOM_RIGHT]  = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY + Ymax     end,
    [POS_BOTTOM_CENTER] = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY + Ymax     end
  }

  -- NOTE: lors de la mise à l'échelle, il se peut que les textes affichés en bordure d'écran sortent. on corrige en les repositionnant dans les limites
  if (drawX > Xmax ) then drawX = Xmax end
  if (drawY > Ymax ) then drawY = Ymax end
  drawX = math.floor(drawX)
  drawY = math.floor(drawY)
  -- debugMessage("drawX, drawY=",drawX, drawY)
  love.graphics.print(pText, drawX, drawY)
  love.graphics.setColor(255, 255, 255, 255) -- RAZ des couleur
end -- displayText

-- ****************************************
-- COLLISIONS
-- ****************************************

--- Vérifie si 2 sprites entre en collision (collision rectangulaire de la taille des sprites)
-- @param pSprite 1er sprite à vérifier
-- @param pSprite 2e sprite à vérifier
-- @return true ou false
function boxCollide (pSprite1, pSprite2)
  ---- debugFunctionEnterNL("boxCollide ", pSprite1, pSprite2) -- ATTENTION  cet appel peut remplir le log
  if (
    (pSprite1 == pSprite2)
    or(pSprite1.status == SPRITE_STATUS.DEAD)
    or(pSprite2.status == SPRITE_STATUS.DEAD)
  )
  then
    return false
  end
  if (pSprite1.x < pSprite2.x + pSprite2.width and
    pSprite2.x < pSprite1.x + pSprite1.width and
    pSprite1.y < pSprite2.y + pSprite2.height and
    pSprite2.y < pSprite1.y + pSprite1.height
    ) then
    return true
  end
  return false
end -- boxCollide