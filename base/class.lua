-- Entité de base pour une classe
-- mise en place d'un principe de POO simplifié par clonage d'objet
-- ****************************************

--- clone un objet (i.e instancie une classe)
-- l'usage le plus courant est
-- local pCloneObject = pBaseObject.new()
-- @param pBaseObject (OPTIONNEL) objet à cloner
-- @param pCloneObject (OPTIONNEL) objet cloné
function new (pBaseObject, pCloneObject)
  if type(pBaseObject) ~= "table" then
    return pCloneObject or pBaseObject
  end
  pCloneObject = pCloneObject or {}
  pCloneObject.__index = pBaseObject
  return setmetatable(pCloneObject, pCloneObject)
end -- new

--- vérifie si un objet est d'une classe donnée
-- l'usage le plus courant est
-- if (isA (pCloneObject,pBaseObject) then
-- @param pBaseObject (OPTIONNEL) objet à cloner
-- @param pCloneObject (OPTIONNEL) objet cloné
function isA (pCloneObject, pBaseObject)
  local pClone_objeO_type = type(pCloneObject)
  local pBaseObject_type = type(pBaseObject)
  if pClone_objeO_type ~= "table" and pBaseObject_type ~= table then
    return pClone_objeO_type == pBaseObject_type
  end
  local index = pCloneObject.__index
  local _isa = index == pBaseObject
  while not _isa and index ~= nil do
    index = index.__index
    _isa = index == pBaseObject
  end
  return _isa
end -- isA

-- objet de base pour toutes les objets pouvant utiliser l'héritage
class = new(table, {new = new, isA = isA})