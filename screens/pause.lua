-- Écran de Pause
-- Ensemble des fonctions de mise a jour, d'affichage et de traitement des entrées
-- ****************************************

--- Actualise l'écran de pause
-- @param pDt delta time
function updatePauseScreen (pDt)
  ---- debugFunctionEnterNL("updatePauseScreen") -- ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  debugInfos = ""
end -- updatePauseScreen

--- Affiche l'écran de pause
function drawPauseScreen ()
  love.graphics.push()
  love.graphics.scale(SPRITE_SCALE, SPRITE_SCALE)

  local content
  local color = {200, 200, 255, 255}
  local h = fontNormal:getHeight("|")

  content = "Jeu en PAUSE"
  displayText(content, fontTitle, POS_CENTER, 0, 0, {255, 160, 0, 255})

  content = "Appuyer sur "..settings.appKeys.pauseGame.." pour relancer le jeu"
  displayText(content, fontNormal, POS_CENTER, 0, h * 2, color)

  content = "Appuyer sur ESC pour quitter"
  displayText(content, fontNormal, POS_CENTER, 0, h * 3, color)

  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.pop()
end -- drawPauseScreen

--- Gestion du "keypressed" sur l'écran de pause
-- @param dynamiques
function keypressedPauseScreen (pKey, pScancode, pIsrepeat)
  ---- debugFunctionEnterNL("keypressedPauseScreen ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
  -- Sortir le jeu du mode pause
  if (pKey == settings.appKeys.pauseGame) then resumeGame() end
end -- keypressedPauseScreen

--- Gestion du "mousepressed" sur l'écran de pause
-- @param dynamiques
function mousepressedPauseScreen (pX, pY, pButton, pIstouch)
  debugFunctionEnter("mousepressedPauseScreen")
  -- rien à faire pour le moment
end -- mousepressedPauseScreen

--- Gestion du "mousemoved" sur l'écran de pause
-- @param dynamiques
function mousemovedPauseScreen (pX, pY, pDX, pDY, pIstouch)
  ---- debugFunctionEnterNL("mousemovedPauseScreen ",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
end -- mousemovedPauseScreen