## GCB_DUNGEON_GENERATOR

### Description
Un simple générateur de donjons aléatoire
Utilise le framework Love2D_Skeleton_mini, une version light du projet Love2D_Skeleton qui ne contient que les éléments de bases pour une application love2D.  

===
### Objectifs
Aucun.

### Mouvements et actions du joueur
Déplacer le joueur: touches ZQSD ou bien les flèches du clavier.  
Mettre en pause: touche P.  
Relancer la partie: touche R.  
Quitter le jeu: clic sur la croix ou touche escape.  

#### en mode debug uniquement  
Confiner la souris dans la fenêtre (ON/OFF): touche F12.  
Activer le mode Debug en live (ON/OFF): touche F9.  
Générer un nouveau donjon: touche F8.  
Prochain Niveau: touche + (pavé numérique).  
Perdre la partie: touche Fin.  
Gagner la partie: touche Debut.  

### Interactions
- Pour le joueur:  
  - collisions avec les bords de la zone de jeu.  
   
### Copyrights
Ecrit en LUA et en utilisant le Framework Love2D.  
Développé sans outil spécifique, en utilisant principalement SublimeText 3 et ZeroBrane Studio (pour le débuggage).  

-----------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)

=================================================

### Description
A simple random dungeon generator
Use Love2D_Skeleton_mini framework, a light version of the Love2D_Skeleton project that only contains the basics for a love2D application.

===
### Goals
Nothing special.  

### Movements and actions of the player
Move the player: moves the mouse.  
Move the player: ZQSD keys or the arrows on the keyboard.  
Action (fire): left click
Pause: P key.  
Restart the game: R key.  
Exit the game: click on the cross or escape key.  

### debug mode only
Confining the mouse in the window (ON/OFF): F12 key.  
Activate the Live Debug mode (ON/OFF): F9 key.  
Next Level: + key (keypad).  
Lose the game: End key.  
Win the game: Home key.  
Generate a new dungeon: F8 key.  

### Interactions
- For the player:
   - collisions with edges of the game area.  
 
### Copyrights
Written in LUA and with the Love2D Framework.  
Developed without any specific tool, mainly using SublimeText 3 and ZeroBrane Studio (for debugging).  

------------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)