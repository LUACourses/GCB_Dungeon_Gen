-- Entité représentant une map
-- Utilise une forme d'héritage simple: dungeon qui hérite de class
-- ****************************************
SHOOW_DOORS = true

CELL_WIDTH = 34
CELL_HEIGHT = 13
DUNGEON_WIDTH = 9
DUNGEON_HEIGHT = 6
DUNGEON_SPACING = 5

ROOM_COUNT = 20 

ROOM_CLOSE = "room_close"
ROOM_OPEN = "room_open"

ROOM_COLORS = {
  close = {128, 128, 128, 255},
  open = {255, 255, 255, 255},
  start = {128, 128, 255, 255}
}

DOOR_CLOSE = "door_close"
DOOR_OPEN = "door_open"

lstRooms = {}

--- Crée un pseudo objet de type map
-- @return un pseudo objet dungeon
function newDungeon ()
  ---- debugFunctionEnter("newDungeon ")

  -- création d'un objet parent
  local lDungeon = class:new()
  
  lDungeon.width = DUNGEON_WIDTH
  lDungeon.height = DUNGEON_HEIGHT
  lDungeon.grid = {}
  lDungeon.x = 5
  lDungeon.y = 5
  lDungeon.cellWidth = CELL_WIDTH
  lDungeon.cellHeight = CELL_HEIGHT
  lDungeon.startRoom = nil
  
  function lDungeon.initialize()
    debugFunctionEnter("lDungeon.initialize")
    lstRooms = {}
    local r, c 
    -- initialise le donjon avec des salles fermée et vide
    for r = 1, lDungeon.height do
      lDungeon.grid[r] = {}
      for c = 1, lDungeon.width  do
        lDungeon.grid[r][c] = lDungeon.createRoom(r, c)
      end
    end
    
    -- initialisation de la salle de départ
    local startCol = math.random(1, lDungeon.width)
    local startRow = math.random(1, lDungeon.height)
    local startRoom = lDungeon.grid[startRow][startCol]
    startRoom.status = ROOM_OPEN
    startRoom.row = startRow
    startRoom.col = startCol
    
    lDungeon.startRoom = startRoom
    
    startRoom.tableIndex = #lstRooms + 1
    table.insert(lstRooms, startRoom)
    
    -- génère le donjon
    -- *******************
    local roomCount = #lstRooms
    local selectedRoomNumber, selectedRoom
    local newRoom
    local newRoomDirection
    local newRoomRow, selectedRoomRow
    local roomFound = false
      
    -- boucle sur le nombre de salles à créer
    while (roomCount < ROOM_COUNT) do
      roomFound = false
      -- sélectionne une salle dans la liste
      selectedRoomNumber = math.random(1, roomCount)
      selectedRoom = lstRooms[selectedRoomNumber]
      selectedRoomRow = selectedRoom.row
      selectedRoomCol = selectedRoom.col
      -- on choisi une direction parmi les 4 possibles à partir de la salle sélectionnée
      newRoomDirection = math.random(1, 4)
      
      --debugMessage(newRoomDirection, selectedRoomRow, selectedRoomCol)
      
      -- on vérifie si la place est possible et si la place est libre
      if (newRoomDirection == 1 and selectedRoomRow > 1) then
        -- vers le haut
        newRoom = lDungeon.grid[selectedRoomRow - 1][selectedRoomCol]
        if (newRoom.status == ROOM_CLOSE) then
          -- ok, la ligne au dessus existe et la salle n'est pas encore ouverte
          selectedRoom.doors["up"] = DOOR_OPEN
          newRoom.doors["down"] = DOOR_OPEN
          roomFound = true
        end
      elseif (newRoomDirection == 2 and selectedRoomCol < lDungeon.width) then
        -- vers la droite
        newRoom = lDungeon.grid[selectedRoomRow][selectedRoomCol + 1]
        if (newRoom.status == ROOM_CLOSE) then
          -- ok, la colonne à droite existe et la salle n'est pas encore ouverte
          selectedRoom.doors["right"] = DOOR_OPEN
          newRoom.doors["left"] = DOOR_OPEN
          roomFound = true
        end
      elseif (newRoomDirection == 3 and selectedRoomRow < lDungeon.height) then
        -- vers le bas
        newRoom = lDungeon.grid[selectedRoomRow + 1][selectedRoomCol]
        if (newRoom.status == ROOM_CLOSE) then
          -- ok, la ligne au desssous existe et la salle n'est pas encore ouverte
          selectedRoom.doors["down"] = DOOR_OPEN
          newRoom.doors["up"] = DOOR_OPEN
          roomFound = true
        end
      elseif (newRoomDirection == 4 and selectedRoomCol > 1) then
        -- vers la gauche
        newRoom = lDungeon.grid[selectedRoomRow][selectedRoomCol - 1]
        if (newRoom.status == ROOM_CLOSE) then
          -- ok, la colonne à gauche existe et la la salle n'est pas encore ouverte
          selectedRoom.doors["left"] = DOOR_OPEN
          newRoom.doors["right"] = DOOR_OPEN
          roomFound = true
        end
      else 
        -- aucune salle ne correspond, on reboucle pour tirer une autre salle de départ
        debugMessage("PAS DE SALLE")
      end -- if (newRoomDirection == 1) then
      -- ok, on a trouvé une nouvelle salle, on l'ajoute à la liste
      if (roomFound) then
        newRoom.status = ROOM_OPEN
        newRoom.tableIndex = roomCount + 1
        table.insert(lstRooms, newRoom)
      end
      roomCount = #lstRooms
    end -- while
  end
  
  function lDungeon.draw()
    ---- debugFunctionEnterNL("lDungeon.draw")
    local color, door
    local posX, posY
    local r, c, p 
    for r = 1, lDungeon.height do
      for c = 1, lDungeon.width do
        -- dessine la salle
        local room = lDungeon.grid[r][c]
        posX = lDungeon.x + (lDungeon.cellWidth + DUNGEON_SPACING) * (c - 1)
        posY = lDungeon.y + (lDungeon.cellHeight + DUNGEON_SPACING) * (r - 1)
        if (room.status == ROOM_OPEN) then 
          color = ROOM_COLORS["open"]
        else
          color = ROOM_COLORS["close"]
        end
        if (room == lDungeon.startRoom) then color = ROOM_COLORS["start"] end

        love.graphics.setColor(color)
        love.graphics.rectangle ("fill", posX, posY, lDungeon.cellWidth, lDungeon.cellHeight)
        
        -- dessine les portes
        if (SHOOW_DOORS) then 
          love.graphics.setColor({255, 0, 0, 255})
          if (room.doors["up"] == DOOR_OPEN) then love.graphics.rectangle ("fill", posX + lDungeon.cellWidth / 2 - 2, posY - 2, 4, 6) end
          if (room.doors["right"] == DOOR_OPEN) then love.graphics.rectangle ("fill", posX + lDungeon.cellWidth - 2, posY + lDungeon.cellHeight / 2 - 2, 6, 4) end
          if (room.doors["down"] == DOOR_OPEN) then love.graphics.rectangle ("fill", posX + lDungeon.cellWidth / 2 - 2, posY + lDungeon.cellHeight - 2, 4, 6) end
          if (room.doors["left"] == DOOR_OPEN) then love.graphics.rectangle ("fill", posX - 2, posY + lDungeon.cellHeight / 2 - 2, 6, 4) end
        end
        love.graphics.setColor({255, 255, 255, 255})
      end
    end
  end

  function lDungeon.createRoom(pRow, pCol)
    local room = {}
    room.row = pRow
    room.col = pCol
    room.status = ROOM_CLOSE
    room.doors = {
      up = DOOR_CLOSE,
      right = DOOR_CLOSE,
      down = DOOR_CLOSE,
      left = DOOR_CLOSE
    }
    return room
  end
  
  lDungeon.initialize()

  return lDungeon
 end -- newDungeon
