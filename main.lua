-- Cette ligne permet d'afficher des traces dans la console pendant l'éxécution
io.stdout:setvbuf("no")

-- requires nécessaires
-- note: l'ordre est important
require("constants")
require(FOLDER_GAME.."/settings")
require(FOLDER_GAME.."/appState")
require(FOLDER_BASE.."/functions")
require(FOLDER_BASE.."/class")
require(FOLDER_BASE.."/sprite")
require(FOLDER_GAME.."/player")
require(FOLDER_GAME.."/pnj")
require(FOLDER_GAME.."/dungeon")

-- fonctions spécifiques à l'application

-- Ensemble des fonctions de mise a jour, d'affichage et de traitement des entrées pour les différents écrans
require("screens/start")
require("screens/play")
require("screens/pause")
require("screens/end")

-- Filtre pour adoucir les contours des images quand elles sont redimentionnées
-- NOTE : à désactiver pour du pixel art ou du style old school ou des facteur d'échelle important
love.graphics.setDefaultFilter("nearest")

-- Cette ligne permet de déboguer pas à pas dans ZeroBraneStudio
if arg[#arg] == "-debug" then require("mobdebug").start() end
-- You can turn the debugging off (require("mobdebug").off()) right after starting debugging (using require("mobdebug").start()) and then turn it on and off around the section you are interested in:
-- require("mobdebug").off() -- <-- turn the debugger off car TROP lent sinon WTF ?

-- variables diverses
-- ******************************
-- VARIABLES POUR DEBUG UNIQUEMENT
-- texte de debug affiché à l'écran par le HUD
debugInfos = ""
-- permet de définir des éléments de code spécifiques à activer ou pas en live avec la touche settings.appKeys.debugSwitch
debugFlagEnabled = false

-- nombre de PNJ à créer
PNJ_COUNT = 50

-- liste des sprites présents dans le jeu
lstSprites = {}

-- Entité représentant le donjon
dungeon = {}
  
-- Entité représentant le joueur
player = {}

-- delai avant l'affichage de l'écran de fin
endScreenCountDowwn = 6

imgAlert = love.graphics.newImage("assets/images/alert.png")
imgDead = love.graphics.newImage("assets/images/dead_1.png")

fontNormal = love.graphics.newFont("assets/fonts/normal.ttf", 15)
fontTitle = love.graphics.newFont("assets/fonts/normal.ttf", 30)

--- Initialiser le jeu
function initializeGame ()
  -- creation des entités
  -- note: l'ordre est important
  -- ******************************
  -- Entité représentant les paramètres du jeu
  settings = newSettings()

  -- Entité représentant l'état du jeu à un instant donné
  appState = newAppState()

end -- initializeGame

--- Menu de démarrage
function startMenu ()
  debugFunctionEnter("startMenu")
  appState.initialize(3) -- niveau 3 pour gagner
  appState.hasStarted = true
  appState.currentScreen = SCREEN_START
end -- startMenu

--- Démarrer le niveau
function startGame ()
  debugFunctionEnter("startGame")

  if (not appState.hasStarted) then
    -- cas ou on n'est pas passé par le startMenu
    appState.initialize(3) -- niveau 3 pour gagner
    appState.hasStarted = true -- important
  end

  lstSprites = {}

  -- création du donjon
  dungeon = newDungeon()
  
  -- creation du joueur
  player = newPlayer(lstSprites)

  player.x = screenWidth / 2
  player.y = screenHeight / 6 * 5

    -- Création des pnj enemies
    --[[
  local index
  for index = 1, PNJ_COUNT do
    local pnj = newPnj(lstSprites)
    pnj.initialize()
  end -- for
  ]]
  appState.currentScreen = SCREEN_PLAY
end -- startGame

--- Mettre l'application en pause
function pauseGame ()
  debugFunctionEnter("pauseGame")
  appState.currentScreen = SCREEN_PAUSE
end -- pauseGame

--- Sortir l'application du mode pause
function resumeGame ()
  debugFunctionEnter("resumeGame")
  appState.currentScreen = SCREEN_PLAY
end -- resumeGame

--- le joueur passe au niveau suivant
-- @param pNewLevel (OPTIONNEL) valeur du nouveau niveau. Si absent, le niveau courant sera incrémenté de 1
function nextLevel (pNewLevel)
  debugFunctionEnter("nextLevel ",pNewLevel)
end -- nextLevel

--- le joueur perd la partie
function loseGame ()
  debugFunctionEnter("loseGame")
  appState.currentScreen = SCREEN_END
end -- loseGame

--- le joueur gagne la partie
function winGame ()
  debugFunctionEnter("winGame")
  appState.currentScreen = SCREEN_END
end -- winGame

--- Quitter l'application
function quitGame ()
  debugFunctionEnter("quitGame")
  love.event.push('quit')
end -- quitGame

--- Love.load pour initialiser l'application
function love.load ()
  -- Initialisation du random avec un temps en ms
  math.randomseed(love.timer.getTime())

  -- La répétition de l'événement love.keypressed est activés lorsqu'une touche est maintenue enfoncée.
  love.keyboard.setKeyRepeat(true)

  -- Active le mode relatif: le curseur est caché et ne se déplace pas lorsque la souris est activée,
  -- mais les événements relatifs du mouvement de la souris sont toujours générés via love.mousemoved
  love.mouse.setRelativeMode(true)

  -- positionne la fenêtre sur l'écran défini pour le debug
  if (DEBUG_MODE > 0) then love.window.setPosition(DEBUG_WINDOWS_X, DEBUG_WINDOWS_Y, DEBUG_DISPLAY_NUM) end

  screenWidth = love.graphics.getWidth() / SPRITE_SCALE
  screenHeight = love.graphics.getHeight() / SPRITE_SCALE

  initializeGame()

  if (appState.currentScreen == SCREEN_START) then startMenu() else startGame() end
end -- love.load

--- Love.update est utilisé pour gérer l'état du jeu frame-to-frame
-- @param pDt delta time
function love.update (pDt)

  -- Actualise le contenu de l'écran
  if (appState.currentScreen == SCREEN_START) then
    -- ******************************
    -- écran de démarrage
    -- ******************************
    updateStartScreen(pDt)
  elseif (appState.currentScreen == SCREEN_PAUSE) then
    -- ******************************
    -- écran de pause
    -- ******************************
    updatePauseScreen(pDt)
  elseif (appState.currentScreen == SCREEN_END) then
    -- ******************************
    -- écran de fin
    -- ******************************
    updateEndScreen(pDt)
  elseif (appState.currentScreen == SCREEN_PLAY) then
    -- ******************************
    -- écran de jeu
    -- ******************************
    updatePlayScreen(pDt)
  end
end -- love.update

--- Love.draw est utilisé pour afficher l'état du jeu sur l'écran.
function love.draw ()
  -- applique l'échelle pour l'affichage
  if (SCREEN_SCALE_X  ~= 1 or SCREEN_SCALE_Y ~= 1) then love.graphics.scale(SCREEN_SCALE_X, SCREEN_SCALE_Y) end

  -- affiche l'écran de jeu
  if (appState.currentScreen == SCREEN_START) then
    -- ******************************
    -- écran de démarrage
    -- ******************************
    drawStartScreen()
  elseif (appState.currentScreen == SCREEN_PAUSE) then
    -- ******************************
    -- écran de pause
    -- ******************************
    drawPauseScreen()
  elseif (appState.currentScreen == SCREEN_END) then
    -- ******************************
    -- écran de fin
    -- ******************************
    drawEndScreen()
  elseif (appState.currentScreen == SCREEN_PLAY) then
    -- ******************************
    -- écran de jeu
    -- ******************************

    -- Affiche le joueur
    -- player.draw() -- fait dans player.drawAll() de drawPlayScreen
    drawPlayScreen()
  end
end -- love.draw

--- intercepte l'appui des touches du clavier
function love.keypressed (pKey, pScancode, pIsrepeat)
  -- debugMessage("touche clavier appuyée:", pKey, pScancode, pIsrepeat)

  -- ÉVÈNEMENTS INDÉPENDANTS DE L'ÉCRAN EN COURS D'AFFICHAGE
  -- ******************************
  -- Quitter l'application
  if (pKey == settings.appKeys.quitGame) then quitGame();return end

  -- Capturer ou non la souris dans la fenêtre
  if (pKey == settings.appKeys.grabMouse) then
    local state = not love.mouse.isGrabbed()
    love.mouse.setGrabbed(state)
    debugMessage("mouse.setGrabbed:", state)
  end

  -- Afficher les infos de debug dans la console
  if (pKey == settings.appKeys.debugSwitch) then
    -- permet de déclencher des évènement de debug en live
    debugFlagEnabled = not debugFlagEnabled
    if (debugFlagEnabled) then
      require("mobdebug").on() -- <-- turn the debugger on
    else
      require("mobdebug").off() -- <-- turn the debugger off
    end
  end

  -- ÉVÈNEMENTS LIÉS À L'ÉCRAN EN COURS D'AFFICHAGE
  -- ******************************
  if (appState.currentScreen == SCREEN_START) then
    -- écran de démarrage
    keypressedStartScreen(pKey, pScancode, pIsrepeat)
  elseif (appState.currentScreen == SCREEN_PAUSE) then
    -- écran de pause
    keypressedPauseScreen(pKey, pScancode, pIsrepeat)
  elseif (appState.currentScreen == SCREEN_END) then
    -- écran de fin
    keypressedEndScreen(pKey, pScancode, pIsrepeat)
  elseif (appState.currentScreen == SCREEN_PLAY) then
    -- écran de jeu
    keypressedPlayScreen(pKey, pScancode, pIsrepeat)
  end
end -- love.keypressed

--- intercepte le relâchement des touches du clavier
function love.keyreleased (pKey)
  -- si une touche figure dans la liste des touche à relacher, on mémorise que cette touche a été relâchée
end -- love.keyreleased

--- intercepte le mouvement de la souris
function love.mousemoved (pX, pY, pDX, pDY, pIstouch)
  -- debugMessage("déplacement de la souris:", pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log

  -- ÉVÈNEMENTS INDÉPENDANTS DE L'ÉCRAN EN COURS D'AFFICHAGE
  -- ******************************

  -- ÉVÈNEMENTS LIÉS À L'ÉCRAN EN COURS D'AFFICHAGE
  -- ******************************
  if (appState.currentScreen == SCREEN_START) then
    -- écran de démarrage
    mousemovedStartScreen(pX, pY, pDX, pDY, pIstouch)
  elseif (appState.currentScreen == SCREEN_PAUSE) then
    -- écran de pause
    mousemovedPauseScreen(pX, pY, pDX, pDY, pIstouch)
  elseif (appState.currentScreen == SCREEN_END) then
    -- écran de fin
    mousemovedEndScreen(pX, pY, pDX, pDY, pIstouch)
  elseif (appState.currentScreen == SCREEN_PLAY) then
    -- écran de jeu
    mousemovedPlayScreen(pX, pY, pDX, pDY, pIstouch)
  end
end -- love.mousemoved

--- intercepte le clic de souris
function love.mousepressed (pX, pY, pButton, pIstouch)
  -- debugMessage("boutons de la souris appuyé:",pX, pY, pButton, pIstouch)

  -- ÉVÈNEMENTS INDÉPENDANTS DE L'ÉCRAN EN COURS D'AFFICHAGE
  -- ******************************

  -- ÉVÈNEMENTS LIÉS À L'ÉCRAN EN COURS D'AFFICHAGE
  -- ******************************
  if (appState.currentScreen == SCREEN_START) then
    -- écran de démarrage
    mousepressedStartScreen(pX, pY, pButton, pIstouch)
  elseif (appState.currentScreen == SCREEN_PAUSE) then
    -- écran de pause
    mousepressedPauseScreen(pX, pY, pButton, pIstouch)
  elseif (appState.currentScreen == SCREEN_END) then
    -- écran de fin
    mousepressedEndScreen(pX, pY, pButton, pIstouch)
  elseif (appState.currentScreen == SCREEN_PLAY) then
    -- écran de jeu
    mousepressedPlayScreen(pX, pY, pButton, pIstouch)
  end
end -- love.mousepressed